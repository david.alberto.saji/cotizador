import React, { useEffect, useState } from 'react'
import './App.css'

const currencyOptions = [
  { value: 'PEN', label: 'Perú' },
  { value: 'COP', label: 'Colombia' },
  { value: 'BRL', label: 'Brasil' },
]

const formatAmount = (amount) => new Intl.NumberFormat().format(amount).replace(/,/g, '.')
const parseAmount = (value) => Number(value.replace(/[^0-9]+/g, ''))

function App() {
  const [amountFrom, setAmountFrom] = useState(0)
  const [amountTo, setAmountTo] = useState(4600)
  const [currency, setCurrency] = useState('PEN')
  const [type, setType] = useState('buy')
  const [amountToFocus, setAmountToFocus] = useState(false)
  const [error, setError] = useState(null)
  const amount = type === 'sell' ? amountFrom : amountTo
  useEffect(() => {
    fetch(`/api/quote?&type=${type}&currency=${currency}&amount=${amount}`)
      .then(res => res.json())
      .then(data => {
        const { cost } = data
        if (type === 'sell') {
          setAmountTo(cost)
        }
        else {
          setAmountFrom(cost)
        }
      })
      .catch((err) => setError(err))
  }, [amount, currency, type])
  const handleAmountFromChange = (event) => {
    const amount = parseAmount(event.target.value)
    setAmountFrom(amount)
  }
  const handleAmountToChange = (event) => {
    const amount = parseAmount(event.target.value)
    setAmountTo(amount)
  }
  const formatedAmountTo = amountToFocus
    ? formatAmount(amountTo)
    : `${formatAmount(amountTo)} ${currency}`
  return (
    <div>
      <div className="quote">
        <label className="radio">
          ENVÍAS
          <input
            type="radio"
            value="sell"
            checked={type === 'sell'}
            onChange={(event) => setType(event.target.value)}
          />
        </label>
        <label className="radio">
          RECIBES
          <input
            type="radio"
            value="buy"
            checked={type === 'buy'}
            onChange={(event) => setType(event.target.value)}
          />
        </label>
        <input
          type="text"
          value={`$${formatAmount(amountFrom)}`}
          disabled={type === 'buy'}
          onChange={handleAmountFromChange}
        />
        <input
          type="text"
          value={formatedAmountTo}
          disabled={type === 'sell'}
          onChange={handleAmountToChange}
          onFocus={() => setAmountToFocus(true)}
          onBlur={() => setAmountToFocus(false)}
        />
        <label>CLP</label>
        <select value={currency} onChange={(event) => setCurrency(event.target.value)}>
          {currencyOptions.map(({ value, label }) => (
            <option value={value}>{label}</option>
          ))}
        </select>
      </div>
      {error && <p className="alarm">Network error!</p>}
    </div>
    
  )
}

export default App
