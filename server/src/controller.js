const USD_VALUE_IN_CLP = 757
const USD_CLP_SPREAD = 0.4

const AVAILABLE_CURRENCIES = {
    PEN: {
        country: 'Perú',
        usdExchangeRate: 0.28,
        spreadPercent: 1.5,
        marginPercent: 3,
    },
    COP: {
        country: 'Colombia',
        usdExchangeRate: 0.00027,
        spreadPercent: 1.8,
        marginPercent: 3.5,
    },
    BRL: {
        country: 'Brasil',
        usdExchangeRate: 0.19,
        spreadPercent: 1.2,
        marginPercent: 2.9,
    }
}

const roundAmount = (amount, currency) => {
    let result
    switch (currency) {
        case 'CLP':
        case 'COP':
            result = Math.round(amount)
            break
        default:
            result = Math.round(amount * 100) / 100
    }
    return result
}

class CurrencyExchanger {
    constructor(amount, currency) {
        this.amount = roundAmount(amount, currency)
        this.currency = currency
    }
    _toUSD() {
        if (this.currency === 'CLP') {
            return new CurrencyExchanger(this.amount / USD_VALUE_IN_CLP, 'USD')
        }
        const currencyData = AVAILABLE_CURRENCIES[this.currency]
        if (!currencyData) {
            throw new Error('Invalid currency')
        }
        const { usdExchangeRate } = currencyData
        return new CurrencyExchanger(this.amount * usdExchangeRate, 'USD')
    }
    _addSpread(spreadPercent) {
        const spread = roundAmount(this.amount * spreadPercent / 100, this.currency)
        return new CurrencyExchanger(this.amount + spread, this.currency)
    }
    _fromUSD(currency) {
        if (this.currency !== 'USD') {
            throw new Error('Currency must be USD')
        }
        if (currency === 'CLP') {
            return new CurrencyExchanger(this.amount * USD_VALUE_IN_CLP, 'CLP')
        }
        const currencyData = AVAILABLE_CURRENCIES[currency]
        if (!currencyData) {
            throw new Error('Invalid currency')
        }
        const { usdExchangeRate } = currencyData
        return new CurrencyExchanger(this.amount / usdExchangeRate, currency)
    }
    _addMargin(marginPercent) {
        const margin = roundAmount(this.amount * marginPercent / 100, this.currency)
        return new CurrencyExchanger(this.amount + margin, this.currency)
    }
    buy() {
        const currencyData = AVAILABLE_CURRENCIES[this.currency]
        if (!currencyData) {
            throw new Error('Invalid currency')
        }
        const { spreadPercent, marginPercent } = currencyData
        return this
            ._toUSD()
            ._addSpread(spreadPercent)
            ._fromUSD('CLP')
            ._addSpread(USD_CLP_SPREAD)
            ._addMargin(marginPercent)
    }
    sell(currency) {
        const currencyData = AVAILABLE_CURRENCIES[currency]
        if (!currencyData) {
            throw new Error('Invalid currency')
        }
        const { spreadPercent, marginPercent } = currencyData
        return this
            ._toUSD()
            ._addSpread(USD_CLP_SPREAD)
            ._fromUSD(currency)
            ._addSpread(spreadPercent)
            ._addMargin(marginPercent)
    }
}

module.exports = CurrencyExchanger