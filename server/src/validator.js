const Joi = require('joi')

const schema = Joi.object({
    amount: Joi.number().min(0).integer().required(),
    type: Joi.string().valid('sell', 'buy').required(),
    currency: Joi.string().valid('PEN', 'COP', 'BRL').required(),
})

exports.validateQuery = (req, res, next) => {
    const { error } = schema.validate(req.query)
    if (error) {
        res.status(400).send({ error: 'Invalid request' })
    } else {
        next()
    }
}