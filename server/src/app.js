const express = require('express')
const path = require('path')

const CurrencyExchanger = require('./controller')
const { validateQuery } = require('./validator')

const app = express()

app.use(express.static(path.join(__dirname, '..','build')))
  
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'build', 'index.html'))
})

app.get('/api/quote', validateQuery, (req, res) => {
  const { amount, type, currency } = req.query
  let result
  if (type === 'buy') {
    const exchanger = new CurrencyExchanger(amount, currency)
    result = exchanger.buy()
  } else {
    const exchanger = new CurrencyExchanger(amount, 'CLP')
    result = exchanger.sell(currency)
  }
  const cost = Math.ceil(result.amount)
  res.send({ cost })
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

module.exports = app