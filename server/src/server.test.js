const request = require('supertest')

const app = require("./app")

test("GET /api/quote", async () => {
    const amount = 4600
    const type = 'buy'
    const currency = 'PEN'
    const response = await request(app)
        .get('/api/quote')
        .query({ amount, type, currency })
        .expect(200)
        .expect('Content-Type', /json/)
    expect(response.body).toEqual({ cost: 1023408 })
})